import random
import csv
import matplotlib.pyplot as plt


def plotit(dict):

    X = list(dict.keys())
    Y = list(dict.values())
    plt.plot(X, Y,'ro')
    plt.show()

def plotit2(list):
    plt.plot(list,'ro')
    plt.show()


def smoothit(dict):
    for _ in range(2):
        for key,value in dict.items():
            try:
                dict[key] = (dict[key-10]+dict[key]+dict[key+10])/3
            except KeyError:
                print(f'Keyerror{key}')
    return dict

def save_csv(row, name):
    with open(name + '.csv', 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(row)
    print(f'Saving {row}')


def read_csv():
    with open('startcarlo.csv') as file:
        csv_reader = csv.reader(file, delimiter=',')
        data = []

        for row_number, row in enumerate(csv_reader):
            print(row[3])
            if row_number < 5:
                data.append(float(row[3]))
            # save_csv(row, 'carlo')
    return data


def simulation():
    ...


def new_capital(rand_n, risk, winning_prob, reward_risk, capital):
    return wintrade(risk, reward_risk, capital) \
        if rand_n < winning_prob else losetrade(risk, capital)


def wintrade(risk, reward_risk, capital):
    return (1 + risk * reward_risk) * capital


def losetrade(risk, capital):
    return (1 - risk) * capital


def winlose(rand_n, winning_prob, win_count):
    return ['W', win_count + 1] if rand_n < winning_prob else ['L', win_count]


def random100():
    return random.randrange(100)


def onetrade(risk, winning_prob, reward_risk, capital, trades, win_count):
    rand_n = random100()
    capital2 = new_capital(rand_n, risk, winning_prob, reward_risk, capital)
    [wl, win_count] = winlose(rand_n, winning_prob, win_count)
    # save_csv([trades, rand_n, round(capital2, 3), wl, win_count], 'carlo')
    return [capital2, win_count]


def createemptydictionary():
    dict = {}
    for x in range(2000,6000,10):
        dict[x] = 0
    return dict


def cleandictionary(dict):
    cleaner = []
    for key, value in dict.items():
        if value == 0:
            cleaner.append(key)
    for key in cleaner:
        dict.pop(key)


def main():
    data = read_csv()
    CAPITAL = data[0]
    RISK = data[1]
    WINNING_PROB = data[2]
    REWARD_RISK = data[3]
    COMISSION = data[4]
    NUMBER_OF_TRADES = 120
    endcapital = {}
    endcapital2=[]
    win_count = 0
    simulationcount = 0
    while simulationcount < 1000:
        capital = CAPITAL
        trades = 0
        while trades < NUMBER_OF_TRADES:
            trades += 1
            [capital, win_count] = onetrade(RISK, WINNING_PROB, REWARD_RISK, capital, trades, win_count)
        simulationcount += 1
        endcapital2.append(capital)
        endcapital[capital] = 1 + endcapital[capital] if endcapital.get(capital) else 1
        # for key in endcapital.keys():
        #     if key <= capital < key + 10:
        #         endcapital[key] += 1
    # cleandictionary(endcapital)
    print(endcapital)
    plotit(endcapital)
    plotit2(sorted(endcapital2))
    print(sorted(endcapital2))





main()

