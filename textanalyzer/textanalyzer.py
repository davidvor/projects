# author = David Vorel
from operator import add
sep = 50 * '-'
TEXTS = ['''
Situated about 10 miles west of Kemmerer,
Fossil Butte is a ruggedly impressive
topographic feature that rises sharply
some 1000 feet above Twin Creek Valley
to an elevation of more than 7500 feet
above sea level. The butte is located just
north of US 30N and the Union Pacific Railroad,
which traverse the valley.''',

         '''At the base of Fossil Butte are the bright
red, purple, yellow and gray beds of the Wasatch
Formation. Eroded portions of these horizontal
beds slope gradually upward from the valley floor
and steepen abruptly. Overlying them and extending
to the top of the butte are the much steeper
buff-to-white beds of the Green River Formation,
which are about 300 feet thick.''',

         '''The monument contains 8198 acres and protects
a portion of the largest deposit of freshwater fish
fossils in the world. The richest fossil fish deposits
are found in multiple limestone layers, which lie some
100 feet below the top of the butte. The fossils
represent several varieties of perch, as well as
other freshwater genera and herring similar to those
in modern oceans. Other fish such as paddlefish,
garpike and stingray are also present.'''
         ]
word = ''
wordcalc = 0
task = ['words',
        'titlecase words',
        'uppercase words',
        'lowercase words',
        'numeric strings']
answers = [0, 0, 0, 0, 0]
calc = 0  # counting up numbers in text
lettercount = [0] * 20  # list for counting letters in words
print('Welcome to the app. Please log in: ')
passwords = {'bob': '123',
             'ann': 'pass123',
             'mike': 'password123',
             'liz': 'pas123'}
enter = False
while not enter:
    user = input('Login: ')
    pas = input('Password:')
    if pas == passwords.get(user):
        enter = True
        print('Welcome to program')
    else:
        print('Your login or password is wrong')
print(sep)
print('We have 3 texts to be analyzed.')
choice = int(input('Enter a number btw. 1 and 3 to select: ')) - 1
print(sep)

for x in range(0,len(TEXTS[choice])):
    letter = TEXTS[choice][x]
    if letter.isdigit() or letter.isalpha() or letter == '-':
        word = word + letter
    elif len(word) == 0:
        word = ''
    else:
        condition = [int(True),
                    int(word[0].isupper()),
                    int(word.isupper()),
                    int(word.islower()),
                    int(word.isnumeric())]
        answers = list(map(add, answers, condition))
        if word.isnumeric():
            calc += int(word)
        lettercount[len(word)] += 1
        word = ''

for x in range(0, 5):
    print(f'There are {answers[x]} {task[x]} in the selected text.')
print(sep)
print(sorted(lettercount.items))
# for x in range(0, 10):
#     if lettercount[x] != 0:
#         if lettercount[x] > 9:
#             lettercountstr = str(lettercount[x])
#         else:
#             lettercountstr = ' ' + str(lettercount[x])
#         print(f' {x} {lettercount[x] * "*"}'
#               f'{(20 - lettercount[x]) * " "} {lettercountstr}')
# for x in range(10, 20):
#     if lettercount[x] != 0:
#         if lettercount[x] > 9:
#             lettercountstr = str(lettercount[x])
#         else:
#             lettercountstr = ' ' + str(lettercount[x])
#         print(f'{x} {lettercount[x] * "*"}'
#               f'{(20 - lettercount[x]) * " "} {lettercountstr}')
print(sep)
print(f'If we summed all the numbers '
      f'in this text we would get: {calc}')
