import csv
import requests
import sys
from bs4 import BeautifulSoup as BS


def save_csv(row, name):
    with open(name + '.csv', 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(row)
        print(f'Saving {row}')


def soup_total_numbers(soup):
    return ','.join(
        clean_spaces(soup.find('td', {'headers': header}).string)
        for header in ('sa2', 'sa3', 'sa6')
    )


def soup_village_name(soup):
    result = ''
    for h3 in soup.find_all('h3'):
        if 'Obec' in str(h3):
            result = h3.text[7:-1]
    return csv_format(result)


def clean_spaces(string):
    return string.replace('\xa0', '')


def soup_party_votes(soup):
    result = ''
    for headers in [{'headers': "t1sa2 t1sb3"},
                    {'headers': "t2sa2 t2sb3"}]:
        for party in soup.find_all('td', headers):
            result += ',' + clean_spaces(party.text)
    return result


def soup_head_parties(soup):
    result = ''
    for headers in [{'headers': "t1sa1 t1sb2"},
                    {'headers': "t2sa1 t2sb2"}]:
        for party in soup.find_all('td', headers):
            result += ',' + clean_spaces(party.text)
    return result


def html_to_soup(address):
    file = requests.get(address)
    return BS(file.text, "html.parser")


def create_address(villnumber, region):
    return 'https://volby.cz/pls/ps2017nss/' \
           'ps311?xjazyk=CZ&xkraj=' + region + '&xobec=' + villnumber


def create_address_prague(praguenumber):
    return 'https://volby.cz/pls/ps2017nss/' \
           'ps311?xjazyk=CZ&xkraj=1&xobec=' + praguenumber + '&xvyber=1100'


def findregion(address):
    return address[address.find('kraj=') + 5]


def find_village_codes(address):
    soup = html_to_soup(address)
    result = []
    for headers in [{'headers': "t1sa1 t1sb1"},
                    {'headers': "t2sa1 t2sb1"},
                    {'headers': "t3sa1 t3sb1"}]:
        for party in soup.find_all('td', headers):
            result.append(clean_spaces(party.text))
    return result


def precinct(village_code, region):
    if region == '1':
        soup = html_to_soup(create_address_prague(village_code))
    else:
        soup = html_to_soup(create_address(village_code, region))
    return soup_village_name(soup) + ',' + \
           soup_total_numbers(soup) + soup_party_votes(soup)


def csv_format(string):
    return ',"' + string + '"'


def table_head(village_code, region):
    result = '"code","location","registered","envelopes","valid"'
    soup = html_to_soup(create_address(village_code, region))
    for headers in [{'headers': "t1sa1 t1sb2"},
                    {'headers': "t2sa1 t2sb2"}]:
        for party in soup.find_all('td', headers):
            result += csv_format(party.text)
    return result


def main(link, end_file):
    village_codes = find_village_codes(link)
    region = findregion(link)
    save_csv([table_head(village_codes[0], region)], end_file)
    for village_code in village_codes:
        try:
            if village_code.isnumeric():
                save_csv([village_code + precinct(village_code, region)], end_file)
        except AttributeError as AE:
            print(f"Error:Data are not valid: {AE.__class__.__name__} "
                  f"village:{create_address(village_code, region)}")


if __name__ == '__main__':
    try:
        main(sys.argv[1], sys.argv[2])
    except IndexError as IE:
        print(f"Error: you didn't give enough parameters: {IE.__class__.__name__}")
    except requests.exceptions.ConnectionError as CE:
        print(f"Error:page is not valid:  {CE.__class__.__name__}")
    except requests.exceptions.MissingSchema as MS:
        print(f"Error:URL is not valid: {MS.__class__.__name__}")

# argv[1] = 'https://volby.cz/pls/ps2017nss/ps32?xjazyk=CZ&xkraj=8&xnumnuts=5201'
# argv[2] = election
# main
#
