from random import sample
import numpy as np


def bullmatrix(row, column):
    """
    Function takes two lists of numbers, that have same length
    returns how many numbers are in both lists and on same position by counting zeros
    it can be used to check if numbers in one parameter are unique (cows !=0)
    """
    column = np.reshape(column, (len(column), 1))
    matrix = row - column
    maindiag = 0
    for i in range(0, len(column)):
        maindiag += 1 if matrix[i, i] == 0 else 0
    other = np.count_nonzero(matrix == 0) - maindiag
    return [maindiag, other]


def taskgen(NUMBERDIGITS):
    return sample(range(0, 9), NUMBERDIGITS)


def control(guess, NUMBERDIGITS):
    if not guess.isnumeric():
        print('Unknown value. Try again')
    elif len(guess) != NUMBERDIGITS:
        print(f'You must write {NUMBERDIGITS} numbers in your guess')


def separate(guess):
    return [int(i) for i in str(guess)]


def samenumbers(guess):
    if bullmatrix(guess, guess)[1] != 0:
        print("You can't repeat numbers")


def report(solution):
    print(f'{solution[0]} bulls, {solution[1]} cows')


def main():
    NUMBERDIGITS = 4
    solution = [0, 0]
    counter = 0
    task = taskgen(NUMBERDIGITS)

    while solution[0] != NUMBERDIGITS:
        guess = input("Enter a number ")
        control(guess, NUMBERDIGITS)
        guess2 = separate(guess)
        samenumbers(guess2)
        solution = bullmatrix(task, guess2)
        report(solution)
        counter += 1

    print(f"Correct, you've guessed the right number in {counter} guesses!")


# -----------------------------------------------------------------------------------

main()
