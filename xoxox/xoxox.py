def tabprint(plane):
    """
    print 3x3 table
    """
    print('-' * 7)
    for i in plane:
        for j in i:
            print(f'|{j}', end='')
        print('|\n' + '-' * 7)


def control():
    """
    checks winning conditions in tic tac toe game
    """
    result = ['', '', '', '']
    for i in range(0, 3):
        result[0] += board[souradnice[0]][i]
        result[1] += board[i][souradnice[1]]
        result[2] += board[i][i]
        result[3] += board[2 - i][i]
    return True if player * 3 in result else False
#kontroluje řádek a sloupec v závislosti na posledním přidaném prvku, hlavní i vedlejší diagonálu vždy
#přišlo mě to jednodušší než kontrolovat, zda je prvek na diagonále


def newgame():
    global table
    global board
    table = list(range(1, 10))
    board = [[' ', ' ', ' '],
             [' ', ' ', ' '],
             [' ', ' ', ' ']]
# tady je ten global ok? teoreticky můžu ty 2 příkazy mít přímo v kodu,
# ale jelikož se opakuje na více místech přišlo mě fajn dát tam funkci

def help2():
    helpboard = ([['1', '2', '3'], ['4', '5', '6'], ['7', '8', '9']])
    for i in range(3):
        for j in range(3):
            if board[i][j] != ' ':
                helpboard[i][j] = board[i][j]
    tabprint(helpboard)


an_game = ''
sep = 26 * '-'
newgame()
player = 'o'
crossroad = {
    'q':'exit()',
    'help':'tabprint([["1", "2", "3"],["4", "5", "6"],["7", "8", "9"]])',
    'help2':'help2()',
    'board':'tabprint(board)'
}
print('Welcome to Tic Tac Toe\n'f'{sep} \nGAME RULES:\n'
      'Each player can place one mark (or stone) per turn on the 3x3 grid\n'
      'The WINNER is who succeeds in placing three of their marks in a\n'
      '* horizontal,\n* vertical or\n*diagonal row\n' + sep)
# v kodu takhle nebo radeji po kazdem \n odentrovat?
tabprint(board)
while table:
    player = 'o' if player == 'x' else 'x'

    while True:
        # dá se to použít takhle? že se čeká dokud není break
        print(sep)
        number = (input(f'Player {player} | Please enter your move number: '))
        if number.isnumeric() and int(number) in table:
            number = int(number)
            break
        elif number in crossroad:
            exec(crossroad.get(number))
        else:
            print(f'Available commands: board, help, q and numbers {table}')

    table.remove(number)
    souradnice = [(number - 1) // 3, (number - 1) % 3]
    board[souradnice[0]][souradnice[1]] = player
    tabprint(board)

    if control():
        print(f'Congratulations, the player {player} WON!')

    elif not table:
        print(f'It\'s a DRAW!')

    if control() or not table:
        an_game = input(f'Do you want another game y/n: ')
        if an_game == 'y':
            newgame()
            tabprint(board)

        else:
            break
